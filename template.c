#include <stdio.h>


#define GREEN "\x1B[36m"
#define KNRM  "\x1B[0m"
#define RED  "\x1B[31m"
#define YELLOW  "\x1B[33m"
#define BLUE  "\x1B[34m"
#define MAGENTA  "\x1B[35m"
#define CYAN  "\x1B[36m"
#define WHITE  "\x1B[37m"

int main()
{
/*insert strings for colors, like so:*/

printf("%s this is green, and %s this is white, this is %s magenta, %s this is cyan, %s this is blue, %s this is yellow, %s this is red \n", GREEN, WHITE, MAGENTA, CYAN, BLUE, YELLOW, RED);
}