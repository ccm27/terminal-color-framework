# terminal-color-framework

Have you ever seen how dull, and lifeless terminal apps look in linux? In tty mode? If only you could add some soft colors....

## Installation

*YOU DO NOT HAVE TO INSTALL ANYTHING, BUT INSERT THIS BIT OF CODE TO THE TOP OF YOUR PROGRAM*

```
#define GREEN "\x1B[36m"
#define KNRM  "\x1B[0m"
#define RED  "\x1B[31m"
#define YELLOW  "\x1B[33m"
#define BLUE  "\x1B[34m"
#define MAGENTA  "\x1B[35m"
#define CYAN  "\x1B[36m"
#define WHITE  "\x1B[37m"

```

## Usage

*insert strings with the color names, as shown in:*
```
#define GREEN "\x1B[36m"
#define KNRM  "\x1B[0m"
#define RED  "\x1B[31m"
#define YELLOW  "\x1B[33m"
#define BLUE  "\x1B[34m"
#define MAGENTA  "\x1B[35m"
#define CYAN  "\x1B[36m"
#define WHITE  "\x1B[37m"
```

**Like so**

```printf("%s Yellow", YELLOW);```